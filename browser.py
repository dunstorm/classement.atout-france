# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.browser import PagesBrowser, URL
from .pages import HotelListingPage, HotelDetailsPage

__all__ = ['ClaBrowser']


class ClaBrowser(PagesBrowser):

    BASEURL = 'https://www.classement.atout-france.fr/'

    hotel_listings = URL(r"/recherche-etablissements\?p_p_id=fr_atoutfrance_classementv2_portlet_facility_FacilitySearch&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_performSearch=1&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_page=(?P<page>\d+)&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_facility_type=1&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_is_luxury_hotel=no", HotelListingPage)
    hotel_details = URL(r"/details-etablissement\?p_p_id=fr_atoutfrance_classementv2_portlet_facility_FacilityPortlet&p_p_lifecycle=0&_fr_atoutfrance_classementv2_portlet_facility_FacilityPortlet_facilityId=(?P<id>\d+)", HotelDetailsPage)

    def __init__(self, *args, **kwargs):
        super(ClaBrowser, self).__init__(*args, **kwargs)

    def iter_hotels(self, page):
        self.hotel_listings.go(page=page)
        assert self.hotel_listings.is_here()
        return self.page.iter_hotels(page=page)

    def get_total_items(self):
        assert self.hotel_listings.is_here()
        return self.page.get_total_items()

    def get_total_pages(self):
        assert self.hotel_listings.is_here()
        return self.page.get_total_pages()

    def go_hotel(self, hotel):
        assert hotel.url
        self.location(hotel.url)
        assert self.hotel_details.is_here()

    def get_hotel(self, hotel):
        assert self.hotel_details.is_here()
        return self.page.get_hotel(hotel)