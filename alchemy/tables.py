from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.mysql import MEDIUMTEXT, VARCHAR, BOOLEAN, DATETIME, LONGTEXT, INTEGER, TINYINT
from sqlalchemy.ext.declarative import declarative_base

from datetime import datetime

Base = declarative_base()


class Task(Base):
    """Result object for sqlalchemy-driven db"""

    __tablename__ = 'task'
    __table_args__ = {
        'mysql_charset': 'utf8mb4',
        'mysql_collate': 'utf8mb4_bin'
    }
    mysql_charset = 'utf8mb4'

    id = Column(INTEGER, primary_key=True)
    url = Column(MEDIUMTEXT, nullable=False)

    total_pages = Column(INTEGER)
    last_page_scraped = Column(INTEGER, default=1)
    total_items = Column(INTEGER)
    last_item_scraped = Column(INTEGER, default=1)
    last_scraping_done = Column(DATETIME, default=datetime.utcnow())

    is_done = Column(TINYINT(1), default=0)


class Hotel(Base):

    __tablename__ = 'hotel'
    __table_args__ = {
        'mysql_charset': 'utf8mb4',
        'mysql_collate': 'utf8mb4_bin'
    }

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    task_id = Column(INTEGER, ForeignKey("task.id"))
    internal_id = Column(INTEGER, unique=True)
    url = Column(MEDIUMTEXT, nullable=False)

    name = Column(MEDIUMTEXT)
    stars = Column(INTEGER)
    type = Column(MEDIUMTEXT)
    address = Column(MEDIUMTEXT)
    zipcode = Column(INTEGER)
    city = Column(MEDIUMTEXT)
    phone = Column(MEDIUMTEXT)
    website = Column(LONGTEXT)
    mail = Column(MEDIUMTEXT)
    page = Column(INTEGER)
    position = Column(INTEGER)
    ranking_date = Column(DATETIME)


def create_all(engine):
    print("creating database")
    Base.metadata.create_all(engine)
