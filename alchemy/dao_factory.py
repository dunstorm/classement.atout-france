from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import Task, create_all

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone
import datetime
from sqlalchemy import and_, or_

from sqlalchemy.orm import scoped_session
from mbackend.tools.db_tools.decorators import dbconnect


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect
    def get_or_create(self, session, unique_task_id, url):
        task_object = session.query(Task).filter(Task.id == unique_task_id).first()
        if not task_object:
            task_object = Task(id=unique_task_id, url=url)
        return task_object