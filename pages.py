from monseigneur.core.browser.pages import HTMLPage
from monseigneur.core.browser.filters.standard import CleanText, CleanDecimal, CleanDate

from monseigneur.modules.public.classement.alchemy.tables import Hotel, Task

import re
from datetime import datetime

class HotelListingPage(HTMLPage):

    def __init__(self, *args, **kwargs):
        HTMLPage.__init__(self, *args, **kwargs)

    def iter_hotels(self, page):
        for hotel in self.doc.xpath("//div[@class='facility-detail--wrapper']"):
            hotel_obj = Hotel()
            url = CleanText(".//a[@class='download-btn see-hotel']/@href")(hotel)
            hotel_obj.internal_id = url.split("facilityId=")[1]
            hotel_obj.url = url
            yield hotel_obj

    def get_total_items(self):
        return CleanDecimal("//div[@class='result-value']", default=0)(self.doc)

    def get_total_pages(self):
        return CleanDecimal("//div[@class='pagination']/a[last()-1]", default=0)(self.doc)

class HotelDetailsPage(HTMLPage):

    def get_hotel(self, hotel):
        hotel.name = CleanText("//h1[@class='facility-detail-title']")(self.doc).upper()
        stars = len(self.doc.xpath("//div[@class='facility-detail-rate']/*"))
        hotel.stars = stars
        hotel.type = CleanText("//div[@class='facility-detail-lead'][1]")(self.doc).upper()
        full_address = CleanText("//div[@class='facility-detail-lead'][2]")(self.doc)

        match = re.search(r"(?P<zipcode>\d{5})", full_address)
        
        if match is None:
            hotel.address = full_address
            hotel.zipcode = None
            hotel.city = None
        else:
            zipcode = match.group('zipcode').strip()
            hotel.zipcode = zipcode
            
            hotel.address = full_address.split(zipcode)[0].upper()
            hotel.city = full_address.split(zipcode)[1].upper()
        
        hotel.phone = CleanText("//a[contains(@class, 'facility-detail-phone')]/div[@class='info-wrapper']/div[2]")(self.doc)
        hotel.website = CleanText("//a[contains(@class, 'facility-detail-site')]/div[@class='info-wrapper']/div[2]")(self.doc)
        hotel.mail = CleanText("//a[contains(@class, 'facility-detail-mail')]/div[@class='info-wrapper']/div[2]")(self.doc)

        ranking_date = CleanText("//div[@class='facility-detail_date']")(self.doc).replace("Date de classement : ", "")
        hotel.ranking_date = datetime.strptime(ranking_date, "%d/%m/%Y")

        return hotel