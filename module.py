from monseigneur.core.tools.backend import Module
from .browser import ClaBrowser

__all__ = ["ClaModule"]


class ClaModule(Module):
    NAME = "Classement-Atout-France"
    MAINTAINER = u"Ritesh"
    EMAIL = "dunstorm@lobstr.io"
    BROWSER = ClaBrowser

    def iter_hotels(self, page):
        return self.browser.iter_hotels(page)

    def get_total_items(self):
        return self.browser.get_total_items()

    def get_total_pages(self):
        return self.browser.get_total_pages()

    def go_hotel(self, hotel):
        return self.browser.go_hotel(hotel)

    def get_hotel(self, hotel):
        return self.browser.get_hotel(hotel)
