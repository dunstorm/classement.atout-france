from mbackend.core.fetcher import Fetcher
from mbackend.core.application import Application
from monseigneur.modules.public.classement.alchemy.dao_manager import DaoManager
from monseigneur.modules.public.classement.alchemy.tables import Task, Hotel
from datetime import datetime

class CalBackend(Application):

    APPNAME = "Application Classement"
    VERSION = '1.0'
    COPYRIGHT = 'Copyright(C) 2020'
    DESCRIPTION = "Scraping Backend for Classement"

    def __init__(self):
        super(CalBackend, self).__init__(self.APPNAME)
        self.setup_logging()
        self.fetcher = Fetcher(is_public=True)
        self.module = self.fetcher.build_backend("classement", params={})

        self.dao = DaoManager("classement")
        self.session, self.scoped_session = self.dao.get_shared_session()

    def main(self):
        unique_task_id = 1
        url = "https://www.classement.atout-france.fr/recherche-etablissements?p_p_id=fr_atoutfrance_classementv2_portlet_facility_FacilitySearch&p_p_lifecycle=0&p_p_mode=view&p_p_state=normal&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_facility_type=1&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_facility_type=2&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_facility_type=3&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_facility_type=4&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_facility_type=5&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_is_luxury_hotel=no&_fr_atoutfrance_classementv2_portlet_facility_FacilitySearch_performSearch=1"
        
        # check if the task object is present
        task_obj = self.session.query(Task).filter(Task.id == unique_task_id).first()
        if not task_obj:
            task_obj = Task(id=unique_task_id, url=url)
            self.session.add(task_obj)
            self.session.commit()
        
        # get the total_pages and total_items
        self.module.iter_hotels(page=1)
        task_obj.total_pages = self.module.get_total_pages()
        task_obj.total_items = self.module.get_total_items()
        
        for page in range(int(task_obj.last_page_scraped), int(task_obj.total_pages) + 1):
            for i, hotel in enumerate(self.module.iter_hotels(page=page)):
                if not self.session.query(Hotel).filter(Hotel.internal_id == hotel.internal_id).count():
                    self.module.go_hotel(hotel=hotel)
                    
                    hotel_obj = self.module.get_hotel(hotel=hotel)
                    hotel_obj.task_id = task_obj.id
                    hotel_obj.page = page
                    hotel_obj.position = (i + 1)

                    task_obj.last_page_scraped = page
                    task_obj.last_item_scraped = (page - 1) * 16 + (i + 1)
                    task_obj.last_scraping_done = datetime.utcnow()
                    
                    if page == int(task_obj.total_pages):
                        task_obj.is_done = True

                    self.session.add(task_obj)
                    self.session.add(hotel_obj)
                    self.session.commit()


if __name__ == '__main__':
    my = CalBackend()
    my.main()
